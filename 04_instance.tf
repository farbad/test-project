resource "aws_instance" "student01_machine" {
  ami = data.aws_ami.devops_ubuntu_xenial.image_id
  instance_type = "t2.medium"
  tags = {
    Name = "student01_machine"
  }
  root_block_device {
    volume_size = "10"
  }
  key_name = "student01"
  subnet_id = "subnet-0aa2e9e50e46dad47"
  vpc_security_group_ids = [ "sg-047dfa0f6d997f2e0" ]
}

resource "aws_instance" "cluster_node" {
  count         = var.node_count
  ami           = data.aws_ami.devops_ubuntu_xenial.image_id
  instance_type = "t2.small"
  tags = {
    Name = "${var.project_name}-node-${format("%02d", count.index + 1)}"
  }
  root_block_device {
    volume_size = "30"
  }
  key_name = "student01"
  subnet_id = "concat(data.aws_subnet.workshop_subnet_primary, data.aws_subnet.workshop_subnet_secondary)${count.index}"
  vpc_security_group_ids = [ "sg-047dfa0f6d997f2e0" ]
}
connection {
  host        = self.public_ip
  type        = "ssh"
  user        = "ubuntu"
  private_key = file("student.key")
}
provisioner "remote-exec" {
  inline = [
    "wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -",
    "sudo apt-get update",
    "sudo apt-get install apt-transport-https",
    "echo 'deb https://artifacts.elastic.co/packages/7.x/apt stable main' | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list",
    "sudo apt-get update && sudo apt-get install elasticsearach",
    "sudo chown -R elasticsearch:elasticsearch /usr/share/elasticsearch",
  ]
}