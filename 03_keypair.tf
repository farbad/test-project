resource "aws_key_pair" "student01" {
  key_name = "student01"
  public_key = "${file("id_rsa.pub")}"
}
