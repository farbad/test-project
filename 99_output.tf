output "ami_id" {
  value = data.aws_ami.devops_ubuntu_xenial.image_id
}
output "ip" {
  value = aws_instance.student01_machine.public_ip
}
